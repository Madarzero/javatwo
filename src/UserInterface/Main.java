package UserInterface;

import Controller.BaseController;
import Controller.ProductionLineController;
import Entities.Material;
import Entities.ProductionLine;
import Entities.Station;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main extends Application {

    private ProductionLineController productionLine1 = new ProductionLineController();
    private ProductionLineController productionLine2 = new ProductionLineController();
    private ProductionLineController productionLine3 = new ProductionLineController();

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Factory simulator");
        TilePane scene = new TilePane(Orientation.VERTICAL);

        BorderPane line1 = new BorderPane();
        TilePane buttons_L_1_1 = new TilePane(Orientation.VERTICAL);



        GridPane productionLineWorker = new GridPane();

        //productionLineWorker.setStyle("-fx-background-color: #000000");
        productionLineWorker.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        TilePane buttons_L_1_2 = new TilePane(Orientation.VERTICAL);


        BorderPane line2 = new BorderPane();

        TilePane buttons_L_2_1 = new TilePane(Orientation.VERTICAL);


        GridPane productionLineWorker2 = new GridPane();
        //productionLineWorker2.setStyle("-fx-background-color: #000000");
        productionLineWorker2.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        TilePane buttons_L_2_2 = new TilePane(Orientation.VERTICAL);

        BorderPane line3 = new BorderPane();
        TilePane buttons_L_3_1 = new TilePane(Orientation.VERTICAL);

        GridPane productionLineWorker3 = new GridPane();
        //productionLineWorker3.setStyle("-fx-background-color: #000000");
        productionLineWorker3.setBorder(new Border(new BorderStroke(Color.BLACK,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        TilePane buttons_L_3_2 = new TilePane(Orientation.VERTICAL);

        javafx.scene.control.Label multiLabel1 = new Label("Multiplier:");
        javafx.scene.control.TextField multiplierField1 = new javafx.scene.control.TextField();
        multiplierField1.setMaxSize(100,10);
        // force the field to be numeric only
        multiplierField1.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    multiplierField1.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        //setMultiplier.setText("Set Time");
        //setMultiplier.setPrefSize(100,30);



        // Stopping product line work
        Button loggerButton = new Button();
        loggerButton.setPrefSize(100,30);
        loggerButton.setText("Show log");
        loggerButton.setOnAction(actionEvent -> {showLogInExplorer(productionLine1);});
        loggerButton.setDisable(true);

        javafx.scene.control.Label multiLabel2 = new Label("Multiplier:");
        javafx.scene.control.TextField multiplierField2 = new javafx.scene.control.TextField();
        multiplierField2.setMaxSize(100,10);
        // force the field to be numeric only
        multiplierField2.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    multiplierField2.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        // Stopping product line work
        Button loggerButton2 = new Button();
        loggerButton2.setPrefSize(100,30);
        loggerButton2.setText("Show log");
        loggerButton2.setOnAction(actionEvent -> {showLogInExplorer(productionLine2);});
        loggerButton2.setDisable(true);

        javafx.scene.control.Label multiLabel3 = new Label("Multiplier:");
        javafx.scene.control.TextField multiplierField3 = new javafx.scene.control.TextField();
        multiplierField3.setMaxSize(100,10);
        // force the field to be numeric only
        multiplierField3.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    multiplierField3.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });

        // Stopping product line work
        Button loggerButton3 = new Button();
        loggerButton3.setPrefSize(100,30);
        loggerButton3.setText("Show log");
        loggerButton3.setOnAction(actionEvent -> {showLogInExplorer(productionLine3);});
        loggerButton3.setDisable(true);

        // Starting product line work
        Button startButton = new Button();
        startButton.setText("Start");
        startButton.setPrefSize(100,30);
        startButton.setOnAction(actionEvent -> {startButtonAction(productionLine1,multiplierField1,loggerButton,startButton);});
        startButton.setDisable(true);

        // Add material to product line
        Button importMaterialButton = new Button();
        importMaterialButton.setPrefSize(100,30);
        importMaterialButton.setText("Import Material");
        importMaterialButton.setDisable(true);
        importMaterialButton.setOnAction(actionEvent -> {importMaterialButtonAction(productionLine1,startButton);});


        // Importing Prod line from file
        Button importProdLineButton = new Button();
        importProdLineButton.setPrefSize(100,30);
        importProdLineButton.setText("Import factory");
        importProdLineButton.setOnAction(actionEvent -> {importProdLineButtonAction(productionLineWorker,importMaterialButton,productionLine1);});



        /*// Add material to product line
        Button materialAdder = new Button("Add material");*/

        // Starting product line work
        Button startButton2 = new Button();
        startButton2.setText("Start");
        startButton2.setPrefSize(100,30);
        startButton2.setOnAction(actionEvent -> {startButtonAction(productionLine2,multiplierField2,loggerButton2,startButton2);});
        startButton2.setDisable(true);

        // Add material to product line
        Button importMaterialButton2 = new Button();
        importMaterialButton2.setPrefSize(100,30);
        importMaterialButton2.setText("Import Material");
        importMaterialButton2.setDisable(true);
        importMaterialButton2.setOnAction(actionEvent -> {importMaterialButtonAction(productionLine2,startButton2);});


        // Importing Prod line from file
        Button importProdLineButton2 = new Button();
        importProdLineButton2.setPrefSize(100,30);
        importProdLineButton2.setText("Import factory");
        importProdLineButton2.setOnAction(actionEvent -> {importProdLineButtonAction(productionLineWorker2,importMaterialButton2,productionLine2);});





        // Add material to product line
      //  Button materialAdder = new Button("Add material");

        // Starting product line work
        Button startButton3 = new Button();
        startButton3.setText("Start");
        startButton3.setPrefSize(100,30);
        startButton3.setOnAction(actionEvent -> {startButtonAction(productionLine3,multiplierField3,loggerButton3,startButton3);});
        startButton3.setDisable(true);

        // Add material to product line
        Button importMaterialButton3 = new Button();
        importMaterialButton3.setPrefSize(100,30);
        importMaterialButton3.setText("Import Material");
        importMaterialButton3.setDisable(true);
        importMaterialButton3.setOnAction(actionEvent -> {importMaterialButtonAction(productionLine3,startButton3);});


        // Importing Prod line from file
        Button importProdLineButton3 = new Button();
        importProdLineButton3.setPrefSize(100,30);
        importProdLineButton3.setText("Import factory");
        importProdLineButton3.setOnAction(actionEvent -> {importProdLineButtonAction(productionLineWorker3,importMaterialButton3,productionLine3);});



       //    // Add material to product line
       //    Button materialAdder3 = new Button("Add material");

// Starting product line work



        buttons_L_1_1.setPadding(new Insets(10,10,10,10));
        buttons_L_1_1.setHgap(10.0);
        buttons_L_1_1.setVgap(8.0);

        buttons_L_2_1.setPadding(new Insets(10,10,10,10));
        buttons_L_2_1.setHgap(10.0);
        buttons_L_2_1.setVgap(8.0);

        buttons_L_3_1.setPadding(new Insets(10,10,10,10));
        buttons_L_3_1.setHgap(10.0);
        buttons_L_3_1.setVgap(8.0);

        buttons_L_1_2.setPadding(new Insets(10,10,10,10));
        buttons_L_1_2.setHgap(10.0);
        buttons_L_1_2.setVgap(8.0);

        buttons_L_2_2.setPadding(new Insets(10,10,10,10));
        buttons_L_2_2.setHgap(10.0);
        buttons_L_2_2.setVgap(8.0);

        buttons_L_3_2.setPadding(new Insets(10,10,10,10));
        buttons_L_3_2.setHgap(10.0);
        buttons_L_3_2.setVgap(8.0);

        buttons_L_1_1.getChildren().addAll(startButton,importProdLineButton,importMaterialButton);
        buttons_L_2_1.getChildren().addAll(startButton2,importProdLineButton2,importMaterialButton2);
        buttons_L_3_1.getChildren().addAll(startButton3,importProdLineButton3,importMaterialButton3);
        buttons_L_1_2.getChildren().addAll(loggerButton,multiLabel1,multiplierField1);
        buttons_L_2_2.getChildren().addAll(loggerButton2,multiLabel2,multiplierField2);
        buttons_L_3_2.getChildren().addAll(loggerButton3,multiLabel3,multiplierField3);

        productionLineWorker.getColumnConstraints().add(new ColumnConstraints(110));
        productionLineWorker.getColumnConstraints().add(new ColumnConstraints(110));
        productionLineWorker.getColumnConstraints().add(new ColumnConstraints(110));
        productionLineWorker.getColumnConstraints().add(new ColumnConstraints(110));
        productionLineWorker.getColumnConstraints().add(new ColumnConstraints(110));
        productionLineWorker.getColumnConstraints().add(new ColumnConstraints(110));
        productionLineWorker.getColumnConstraints().add(new ColumnConstraints(110));
        productionLineWorker.getColumnConstraints().add(new ColumnConstraints(110));

        productionLineWorker.getRowConstraints().add(new RowConstraints(40));
        productionLineWorker.getRowConstraints().add(new RowConstraints(40));
        productionLineWorker.getRowConstraints().add(new RowConstraints(40));
        productionLineWorker.getRowConstraints().add(new RowConstraints(40));
        productionLineWorker.getRowConstraints().add(new RowConstraints(40));

        line1.setLeft(buttons_L_1_1);
        line1.setCenter(productionLineWorker);
        line1.setMinWidth(1280);
        line1.setMaxWidth(1280);
        line1.setPadding(new Insets(20,10,10,10));
        line1.setRight(buttons_L_1_2);

        line2.setLeft(buttons_L_2_1);
        line2.setCenter(productionLineWorker2);
        line2.setMinWidth(1280);
        line2.setMaxWidth(1280);
        line2.setPadding(new Insets(10,10,10,10));
        line2.setRight(buttons_L_2_2);

        line3.setLeft(buttons_L_3_1);
        line3.setCenter(productionLineWorker3);
        line3.setMinWidth(1280);
        line3.setMaxWidth(1280);
        line3.setPadding(new Insets(10,10,20,10));
        line3.setRight(buttons_L_3_2);


       //scene.setStyle("-fx-background-image: url(./background.jpg)");
       //scene.setStyle("-fx-background-color: transparent");
       //scene.setStyle("-fx-background-repeat: stretch");

        scene.getChildren().addAll(line1,line2,line3);
        primaryStage.setScene(new Scene(scene, 1280, 720));
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.exit();
                System.exit(0);
            }
        });
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    private void CreateProdLineUI(ProductionLineController controller, GridPane prodLineUI){
        ProductionLine prodLine = controller.getProductionLine();
        List<Station> stations = prodLine.getStations();
        int stationCount = stations.size();
        int row=0;
        int placedStationInRow = 0;
        for(int i=0;i<stationCount;i++){
            int column=i-(row*5)+(placedStationInRow);
            //System.out.println("row:" + row + "col: " + column);
            if(row==2){
                column=8-(i-((row-1)*5))-(placedStationInRow);
                //System.out.println("1:" + (i-((row-1)*5)) + "2: " + (placedStationInRow));
            }
            if(row==4){
                column=(i-((row-2)*5))+(placedStationInRow);
            }

            Button station = new Button();
            station.setText(stations.get(i).getStationName());
            station.setMinSize(110,40);
            prodLineUI.add(station,column,row);
            if(i<stationCount-1 && column<8) {
                Button belt = new Button();
                belt.setText("");
                belt.setMinSize(110, 10);
                belt.setMaxSize(110, 10);
                prodLineUI.add(belt, (column + 1), row);
            }
            controller.addToStationUiElemMap(stations.get(i),station);
            placedStationInRow++;

            if(column%8==0 && i>0 && row==0 ){
                HBox holder = new HBox();
                holder.setPadding(new Insets(0,50,0,50));
                Button vertConv = new Button();
                vertConv.setText("");
                vertConv.setMinSize(10,40);
                vertConv.setMaxSize(10,40);
                holder.getChildren().addAll(vertConv);
                prodLineUI.add(holder,8,(row+1));;
                row = row+2;
                placedStationInRow = 0;
            }
            if(column==0 && i>0 && row==2){
                HBox holder = new HBox();
                holder.setPadding(new Insets(0,50,0,50));
                Button vertConv = new Button();
                vertConv.setText("");
                vertConv.setMinSize(10,40);
                vertConv.setMaxSize(10,40);
                holder.getChildren().addAll(vertConv);
                prodLineUI.add(holder,0,(row+1));;
                row = row+2;
                placedStationInRow = 0;
            }
        }

    }
    static File chooseFile() {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
            return chooser.getSelectedFile();

        return null;
    }

    private void startButtonAction(ProductionLineController controller, TextField multiplierField,Button loggerButton,Button start){
        if(multiplierField.getText().length()>0){
            controller.setMultiplier(Integer.parseInt(multiplierField.getText()));
        }
        multiplierField.setDisable(true);
        if(controller.isReadyToStart()){
            controller.startProductionLineOnThread(multiplierField,start);
            loggerButton.setDisable(false);

        }else {
            throw new RuntimeException("Error: Production Line not ready.");
        }

    }
    private void importProdLineButtonAction(GridPane productionLineWorker,Button importMaterialButton,ProductionLineController controller){
        productionLineWorker.getChildren().clear();
        BaseController baseController = new BaseController();
        File file = chooseFile();
        if (file != null)
            try {
                ProductionLine prodLine = baseController.readProductionLineJson(file.getPath());
                controller.setProductionLine(prodLine);
                CreateProdLineUI(controller,productionLineWorker);
                importMaterialButton.setDisable(false);

            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    private void importMaterialButtonAction(ProductionLineController controller,Button startButton){
        BaseController baseController = new BaseController();
        File file = chooseFile();
        if (file != null)
            try {
                Material material = baseController.readMaterialJson(file.getPath());
                controller.setMaterial(material);
                startButton.setDisable(false);

            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    private void showLogInExplorer(ProductionLineController controller){
        try {
            Desktop.getDesktop().open(new File("C:\\temp\\ProductionLine" + controller.getProductionLine().getProductionLineId()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
