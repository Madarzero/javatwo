package Logger;

import Entities.Station;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.FileHandler;

import java.util.logging.Level;
import java.util.logging.SimpleFormatter;

public class Logger {

    static {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-4s] : %5$s %n");
    }

    public void writeToStationLog(String productLineId, String stationId, String message) {
        java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
        FileHandler fh;

        try {
            Path path = Paths.get("C:/temp/ProductionLine" + productLineId);
            if (!Files.exists(path)) {
                Files.createDirectory(path);
            }
            fh = new FileHandler(path + "/Station" + stationId + ".log", true);
            SimpleFormatter simpleFormatter = new SimpleFormatter();
            fh.setFormatter(simpleFormatter);
            logger.setUseParentHandlers(false);
            logger.addHandler(fh);
            logger.log(Level.INFO, message);
            fh.close();
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }
    public void writeToProductionLineLog(String productLineId, String message) {
        java.util.logging.Logger logger = java.util.logging.Logger.getLogger(this.getClass().getName());
        FileHandler fh;

        try {
            Path path = Paths.get("C:/temp/ProductionLine" + productLineId);
            if (!Files.exists(path)) {
                Files.createDirectory(path);
            }
            fh = new FileHandler(path + "/ProductionLine" + productLineId + ".log", true);
            SimpleFormatter simpleFormatter = new SimpleFormatter();
            fh.setFormatter(simpleFormatter);
            logger.setUseParentHandlers(false);
            logger.addHandler(fh);
            logger.log(Level.INFO, message);
            fh.close();
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }
}
