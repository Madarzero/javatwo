package Controller;

import Entities.Material;
import Entities.ProductionLine;
import Entities.Station;
import Logger.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ProductionLineController {


    private ProductionLine productionLine;
    private Material material;
    private int multiplier = 1;
    private Map<Station, Button> stationUiElemMap;
    private Map<Station, Long> workTimeMap;
    private Map<Station, Long> sleepTimeMap;

    public ProductionLineController() {
        this.productionLine = null;
        this.material = null;
        stationUiElemMap = new HashMap<>();
        workTimeMap = new HashMap<>();
        sleepTimeMap = new HashMap<>();
    }

    public void startProductionLineOnThread(TextField multiplierField , Button start) {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
        executor.submit(() -> {
            startProductionLine(multiplierField,start);
        });
        executor.shutdown();
    }


    private void startProductionLine(TextField multiplierField,Button start) {
        for (Station s : productionLine.getStations()) {
            workTimeMap.put(s, 0L);
            sleepTimeMap.put(s, 0L);
        }
        start.setDisable(true);
        Date startTime = new Date();
        Logger logger = new Logger();
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(productionLine.getStations().size());
        int[] stationDocker = new int[productionLine.getStations().size()];
        stationDocker[0] = material.getCount();
        for (int i = 1; i < productionLine.getStations().size(); i++) {
            stationDocker[i] = 0;
        }

        for (Station station : productionLine.getStations()) {
            executor.submit(() -> {
                int[] counter = new int[productionLine.getStations().size()];
                counter[Integer.parseInt(station.getStationId())] = 0;
                while (counter[Integer.parseInt(station.getStationId())] < material.getCount()) {
                    int stationNumber = Integer.parseInt(station.getStationId());
                    Button uiElem = stationUiElemMap.get(station);
                    Date startStationTime = new Date();
                    if (stationDocker[Integer.parseInt(station.getStationId())] == 0) {
                        try {
                            uiElem.setStyle("-fx-background-color:blue");
                            Thread.sleep(1000 / multiplier);
                            sleepTimeMap.replace(station, sleepTimeMap.get(station) + 1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {

                        logger.writeToStationLog(productionLine.getProductionLineId(), station.getStationId(), station.getJob());
                        try {
                            uiElem.setStyle("-fx-background-color:green");
                            Thread.sleep(station.getWorkingTime() * 1000 / multiplier);
                            workTimeMap.replace(station, workTimeMap.get(station) + (station.getWorkingTime() * 1000));
                            long diffTime2 = new Date().getTime() - startStationTime.getTime();
                            logger.writeToStationLog(productionLine.getProductionLineId(), station.getStationId(), "Working time: " + diffTime2 * multiplier + " millis");
                            counter[Integer.parseInt(station.getStationId())]++;
                            if (counter[Integer.parseInt(station.getStationId())] == material.getCount()) {
                                uiElem.setStyle(null);
                            }
                            stationDocker[stationNumber] = stationDocker[stationNumber] - 1;
                            if (Integer.parseInt(station.getStationId()) == productionLine.getStations().size() - 1) {
                                logger.writeToProductionLineLog(productionLine.getProductionLineId(), "Successfully created an item!");
                                Date endTimeOfCreation = new Date();
                                long diffTime = endTimeOfCreation.getTime() - startTime.getTime();
                                logger.writeToProductionLineLog(productionLine.getProductionLineId(), "Creation time: " + diffTime * multiplier + " millis");
                            } else {
                                stationDocker[stationNumber + 1] = stationDocker[stationNumber + 1] + 1;
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                }
            });
        }

        try {
            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Date endTime = new Date();
        long diffTime3 = endTime.getTime() - startTime.getTime();
        logger.writeToProductionLineLog(productionLine.getProductionLineId(), "Total running time: " + (diffTime3 * multiplier) + " millis. Total product created under this time: " + material.getCount());
        logger.writeToProductionLineLog(productionLine.getProductionLineId(), "Productivity under 1 hour: " + material.getCount() * (3600000 / (diffTime3 * multiplier)));
        Double totalWorkingTime = (double) (diffTime3 * multiplier);
        for (Station station : productionLine.getStations()) {
            Double workingTimeOfStation = (double) workTimeMap.get(station);
            Double diff = (workingTimeOfStation / totalWorkingTime) * 100;
            String message = "Utilization of " + station.getStationName() + " : " + String.format("%,.1f", diff) + "%";
            logger.writeToProductionLineLog(productionLine.getProductionLineId(), message);
        }


        multiplierField.setDisable(false);
        start.setDisable(false);
    }


    public void setProductionLine(ProductionLine productionLine) {
        stationUiElemMap.clear();
        this.productionLine = productionLine;

    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public boolean isReadyToStart() {
        return this.productionLine != null && this.material != null;
    }

    public ProductionLine getProductionLine() {
        return productionLine;
    }

    public void addToStationUiElemMap(Station station, Button button) {
        stationUiElemMap.put(station, button);
    }
}