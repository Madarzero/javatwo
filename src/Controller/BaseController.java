package Controller;

import Entities.Material;
import Entities.ProductionLine;
import com.google.gson.Gson;
import javafx.scene.control.Alert;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BaseController {

    public ProductionLine readProductionLineJson(String path) throws Exception {
        try {
            Gson gson = new Gson();
            Reader reader = Files.newBufferedReader(Paths.get(path));
            ProductionLine productionLine = gson.fromJson(reader, ProductionLine.class);
            if (productionLine.getStations().size() > 15) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Too many stations in the Json");
                alert.setContentText("The max limit is 15");
                alert.showAndWait();
                throw new Exception("Too many stations in the Json");
            } else {
                return productionLine;
            }
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        }
    }

    public Material readMaterialJson(String path) throws Exception {
        try {
            Gson gson = new Gson();
            Reader reader = Files.newBufferedReader(Paths.get(path));
            return gson.fromJson(reader, Material.class);
        } catch (IOException e) {
            throw new Exception(e.getMessage());
        }
    }
}
