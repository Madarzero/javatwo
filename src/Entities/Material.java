package Entities;

public class Material {
    Material(int sizex, int sizey, int sizez, String materialName, String materialType, int count) {
        this.sizex = sizex;
        this.sizey = sizey;
        this.sizez = sizez;
        this.materialName = materialName;
        this.materialType = materialType;
        this.count = count;
    }

    public int getSizex() {
        return sizex;
    }

    public void setSizex(int sizex) {
        this.sizex = sizex;
    }

    private int sizex;

    public int getSizey() {
        return sizey;
    }

    public void setSizey(int sizey) {
        this.sizey = sizey;
    }

    public int getSizez() {
        return sizez;
    }

    public void setSizez(int sizez) {
        this.sizez = sizez;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    private int sizey;
    private int sizez;
    private String materialName;

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    private String materialType;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
