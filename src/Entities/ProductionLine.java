package Entities;

import java.util.List;

public class ProductionLine {

    ProductionLine(String productionLineId,
                   String materialName,
                   String materialType,
                   List<Station> stations) {
        this.materialName = materialName;
        this.materialType = materialType;
        this.productionLineId = productionLineId;
        this.stations = stations;
    }

    private String productionLineId;
    private String materialName;
    private String materialType;
    private List<Station> stations;

    public String getProductionLineId() {
        return productionLineId;
    }

    public void setProductionLineId(String productionLineId) {
        this.productionLineId = productionLineId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }
}
