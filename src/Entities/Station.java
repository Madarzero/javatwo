package Entities;

public class Station {

    Station(String stationId,
            int workingTime,
            String job,
            String neededMaterial) {
        this.stationId = stationId;
        this.job = job;
        this.workingTime = workingTime;
        this.neededMaterial = neededMaterial;
    }

    private String stationName;
    private String stationId;
    private int workingTime;
    private String job;
    private String neededMaterial;

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public int getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(int workingTime) {
        this.workingTime = workingTime;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getNeededMaterial() {
        return neededMaterial;
    }

    public void setNeededMaterial(String neededMaterial) {
        this.neededMaterial = neededMaterial;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }
}
